from .analysis.analyser import Analyser
from ._version import get_version

_VERSION = get_version()
__version__ = _VERSION
