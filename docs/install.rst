Install
=======

``actomyosin_analyser`` can be installed via pip:

.. code:: bash

   pip install actomyosin-analyser

Docker
------

``actomyosin_analyser`` is also installed in the ``bead_state_model``
`docker image <https://hub.docker.com/r/ilyask/bead_state_model>`_. Details how to install and use it
can be found in the
`documentation <http://akbg.uni-goettingen.de/docs/bead_state_model/>`_ of ``bead_state_model``.
